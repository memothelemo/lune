use lune::ast::AstToken;

static CODE: &str = "local function that	() end";

fn main() {
    let tokens = lune::syntax::tokenizer::tokenize(CODE).unwrap();
    let tokens = lune::ast::to_syntax_tokens(tokens);
    println!("{:#?}", tokens);
}
