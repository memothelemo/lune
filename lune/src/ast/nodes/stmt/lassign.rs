use super::*;

pub type LocalAssignNameList = NodeList<SyntaxToken>;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct LocalAssignment {
    pub names: LocalAssignNameList,
    pub exprlist: ExprList,
}

impl AstNode for LocalAssignment {
    fn children(&self) -> Vec<&dyn AstNode> {
        let mut children = self.names.children();
        for child in self.exprlist.children() {
            children.push(child);
        }
        children
    }

    fn location(&self) -> Location {
        todo!()
    }
}
