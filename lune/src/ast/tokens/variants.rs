use super::*;

macro_rules! call_like_struct {
    ($name:ident, $typ:pat) => {
        #[derive(Clone, PartialEq)]
        pub struct $name<'a>(&'a SyntaxToken);

        impl<'a> AstToken<'a> for $name<'a> {
            fn token(&self) -> &SyntaxToken {
                &self.0
            }

            fn can_cast(typ: &'_ TokenType) -> bool {
                matches!(typ, $typ)
            }

            fn cast(token: &'a SyntaxToken) -> Option<Self> {
                if Self::can_cast(token.token_type()) {
                    Some($name(token))
                } else {
                    None
                }
            }
        }

        impl std::fmt::Debug for $name<'_> {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
                self.0.fmt(f)
            }
        }

        impl std::fmt::Display for $name<'_> {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
                write!(f, "{}", self.0)
            }
        }
    };
}

call_like_struct!(Name, TokenType::Identifier(..));

#[allow(dead_code)]
impl<'a> Name<'a> {
    pub fn name(&self) -> String {
        match self.0.token_type() {
            TokenType::Identifier(a) => a.to_string(),
            _ => panic!("Expected identifier"),
        }
    }
}

call_like_struct!(
    Bool,
    TokenType::Symbol(SymbolType::True | SymbolType::False)
);

#[allow(dead_code)]
impl<'a> Bool<'a> {
    pub fn value(&self) -> bool {
        match self.0.token_type() {
            TokenType::Symbol(SymbolType::True) => true,
            TokenType::Symbol(SymbolType::False) => false,
            _ => panic!("Expected boolean"),
        }
    }
}

call_like_struct!(Nil, TokenType::Symbol(SymbolType::Nil));

call_like_struct!(Number, TokenType::Number(..));

#[allow(dead_code)]
impl<'a> Number<'a> {
    pub fn value(&self) -> String {
        match self.0.token_type() {
            TokenType::Number(n) => n.to_string(),
            _ => panic!("Expected number!"),
        }
    }

    pub fn to_f64(&self) -> Result<f64, std::num::ParseFloatError> {
        self.value().parse::<f64>()
    }
}

call_like_struct!(Str, TokenType::String { .. });

#[allow(dead_code)]
impl<'a> Str<'a> {
    pub fn value(&self) -> String {
        match self.0.token_type() {
            TokenType::String { content, .. } => content.to_string(),
            _ => panic!("Expected string!"),
        }
    }

    pub fn quote(&self) -> StringQuoteType {
        match self.0.token_type() {
            TokenType::String { quote, .. } => quote.clone(),
            _ => panic!("Expected string!"),
        }
    }
}

call_like_struct!(Varargs, TokenType::Symbol(SymbolType::TripleDot));

#[allow(dead_code)]
impl<'a> Varargs<'a> {
    pub fn value(&self) -> String {
        "...".into()
    }
}
