mod expr;
mod list;
mod stmt;
mod traits;

pub use expr::*;
pub use list::*;
pub use traits::*;
pub use stmt::*;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct Block {
    pub stmts: Stmts,
    pub last_stmt: Option<LastStmt>,
}

impl AstNode for Block {
    fn children(&self) -> Vec<&dyn AstNode> {
        let mut children = self.stmts.children();
        if let Some(last_stmt) = &self.last_stmt {
            for child in last_stmt.children() {
                children.push(child);
            }
        }
        children
    }

    fn location(&self) -> super::Location {
        let start = self.stmts.location().start;
        super::Location {
            start,
            end: self.last_stmt.as_ref()
                .map(|v| v.location().end)
                .unwrap_or(self.stmts.location().end),
        }
    }
}
