use super::*;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum TableField {
    ExprIndex {
        location: Location,
        index: Box<Expr>,
        value: Box<Expr>,
    },
    Expr(Box<Expr>),
    NameIndex {
        name: SyntaxToken,
        value: Box<Expr>,
    },
}

impl AstNode for TableField {
    fn children(&self) -> Vec<&dyn AstNode> {
        match self {
            TableField::ExprIndex { index, value, .. } => {
                vec![index.as_ref(), value.as_ref()]
            },
            TableField::Expr(expr) => vec![expr.as_ref()],
            TableField::NameIndex { name, value } => {
                vec![name, value.as_ref()]
            },
        }
    }

    fn location(&self) -> Location {
        match self {
            TableField::ExprIndex { location, .. } => *location,
            TableField::Expr(node) => node.location(),
            TableField::NameIndex { name, value } => {
                Location::new(
                    name.start_position(),
                    value.location().end
                )
            },
        }
    }
}

pub type TableFieldList = NodeList<TableField>;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct TableCtor {
    pub location: Location,
    pub fields: TableFieldList,
}

impl AstNode for TableCtor {
    fn children(&self) -> Vec<&dyn AstNode> {
        self.fields.children()
    }

    fn location(&self) -> Location {
        self.location
    }
}
