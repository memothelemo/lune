use super::*;

pub trait AstToken<'a> {
    fn token(&self) -> &SyntaxToken;

    fn can_cast(typ: &TokenType) -> bool;
    fn cast(token: &'a SyntaxToken) -> Option<Self>
    where
        Self: Sized;

    fn start_position(&self) -> Position {
        self.token().token.start_position
    }

    fn end_position(&self) -> Position {
        self.token().token.end_position
    }
}
