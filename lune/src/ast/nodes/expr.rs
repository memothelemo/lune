use crate::ast::*;
use traits::AstNode;

use super::*;

mod binary;
mod fexpr;
mod suffix;
mod table;
mod unary;
mod value;
mod vars;

pub use binary::*;
pub use suffix::*;
pub use table::*;
pub use unary::*;
pub use value::*;
pub use vars::*;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum Expr {
    Binary(Binary),
    Unary(Unary),
    Value(Value),
}

impl AstNode for Expr {
    fn children(&self) -> Vec<&dyn AstNode> {
        match self {
            Expr::Binary(node) => node.children(),
            Expr::Value(node) => node.children(),
            Expr::Unary(node) => node.children(),
        }
    }

    fn location(&self) -> Location {
        match self {
            Expr::Binary(node) => node.location(),
            Expr::Value(node) => node.location(),
            Expr::Unary(node) => node.location(),
        }
    }
}
