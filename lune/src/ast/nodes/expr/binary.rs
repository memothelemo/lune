use super::*;
use crate::operator;

operator! {
    #[allow(dead_code)]
    #[derive(Debug, Clone)]
    pub enum BinopKind {
        NilshCoalescing => 11,
        Exponent => 10,
        Multiply => 7,
        FloorDivision => 7,
        Divide => 7,
        Modulo => 7,
        Add => 6,
        Subtract => 6,
        Concat => 5,
        Equality => 3,
        Inequality => 3,
        GreaterThan => 3,
        GreaterEqual => 3,
        LessThan => 3,
        LessEqual => 3,
        And => 2,
        Or => 1,
    }
    is_right_associate = | k: &BinopKind | {
        matches!(k, Self::Concat | Self::Exponent)
    }
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct BinaryOperator {
    pub kind: BinopKind,
    pub token: SyntaxToken,
}

impl AstNode for BinaryOperator {
    fn children(&self) -> Vec<&dyn AstNode> {
        self.token.children()
    }

    fn location(&self) -> Location {
        self.token.location()
    }
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct Binary {
    pub lhs: Box<Expr>,
    pub op: BinaryOperator,
    pub rhs: Box<Expr>,
}

impl AstNode for Binary {
    fn children(&self) -> Vec<&dyn AstNode> {
        vec![&*self.lhs, &*self.rhs]
    }

    fn location(&self) -> Location {
        Location::new(self.lhs.location().start, self.rhs.location().end)
    }
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct Unary {
    pub op: UnaryOperator,
    pub expr: Box<Expr>,
}

impl AstNode for Unary {
    fn children(&self) -> Vec<&dyn AstNode> {
        vec![&*self.expr]
    }

    fn location(&self) -> Location {
        Location::new(self.op.location().start, self.expr.location().end)
    }
}
