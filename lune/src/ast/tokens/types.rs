use super::*;

lazy_enum! {
    #[derive(Clone, PartialEq)]
    pub enum SymbolType {
        And => "and",
        Break => "break",
        Do => "do",
        Else => "else",
        ElseIf => "elseif",
        End => "end",
        False => "false",
        For => "for",
        Function => "function",
        If => "if",
        In => "in",
        Local => "local",
        Nil => "nil",
        Not => "not",
        Or => "or",
        Repeat => "repeat",
        Return => "return",
        Then => "then",
        True => "true",
        Until => "until",
        While => "while",

        TripleDot => "...",
        DoubleDot => "..",
        Dot => ".",

        GreaterEqual => ">=",
        LessEqual => "<=",
        DoubleEqual => "==",
        TildeEqual => "~=",

        GreaterThan => ">",
        LessThan => "<",
        Equal => "=",

        OpenParen => "(",
        CloseParen => ")",

        OpenBracket => "[",
        CloseBracket => "]",

        OpenCurly => "{",
        CloseCurly => "}",

        Semicolon  => ";",
        Colon => ":",
        Comma => ",",

        Cross => "+",
        Dash => "-",
        Asterisk => "*",
        Slash => "/",
        Percent => "%",
        Caret => "^",
        Hash => "#",
    }

    type = TokenType::Symbol;
}

#[derive(Debug, Clone, PartialEq)]
pub enum StringQuoteType {
    Brackets(usize),
    Double,
    Single,
}

impl StringQuoteType {
    pub fn start_quote(&self) -> String {
        match self {
            StringQuoteType::Brackets(equals) => format!("[{}[", "=".repeat(*equals)),
            StringQuoteType::Double => "\"".into(),
            StringQuoteType::Single => "'".into(),
        }
    }

    pub fn end_quote(&self) -> String {
        match self {
            StringQuoteType::Brackets(equals) => format!("]{}]", "=".repeat(*equals)),
            StringQuoteType::Double => "\"".into(),
            StringQuoteType::Single => "'".into(),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum CommentType {
    Multiline(usize),
    Single,
}

impl CommentType {
    pub fn start_quote(&self) -> String {
        match self {
            CommentType::Multiline(equals) => format!("--[{}[", "=".repeat(*equals)),
            CommentType::Single => "--".into(),
        }
    }

    pub fn end_quote(&self) -> String {
        match self {
            CommentType::Multiline(equals) => format!("]{}]", "=".repeat(*equals)),
            CommentType::Single => "'".into(),
        }
    }
}
