#[macro_use]
mod macros;
mod nodes;
mod tokens;

pub use macros::*;
pub use nodes::*;
pub use tokens::*;

use smol_str::SmolStr;

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq)]
pub struct Location {
    pub start: Position,
    pub end: Position,
}

impl Location {
    pub fn new(start: Position, end: Position) -> Self {
        Location { start, end }
    }

    #[inline]
    pub fn empty() -> Self {
        Location {
            start: Position::empty(),
            end: Position::empty(),
        }
    }
}

impl PartialOrd for Location {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.start.offset.partial_cmp(&other.end.offset)
    }
}

impl std::fmt::Display for Location {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}-{}", self.start, self.end)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Position {
    pub line: usize,
    pub column: usize,
    pub offset: usize,
}

#[allow(dead_code)]
impl Position {
    pub fn new(line: usize, column: usize, offset: usize) -> Self {
        Position {
            line,
            column,
            offset,
        }
    }

    #[inline]
    pub fn empty() -> Self {
        Position {
            line: 0,
            column: 0,
            offset: 0,
        }
    }
}

impl Default for Position {
    fn default() -> Self {
        Position {
            line: 1,
            column: 1,
            offset: 0,
        }
    }
}

impl PartialOrd for Position {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.offset.partial_cmp(&other.offset)
    }
}

impl std::fmt::Display for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{}", self.line, self.column)
    }
}
