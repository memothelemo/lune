use super::*;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum Value {
    Bool(SyntaxToken),
    Nil(SyntaxToken),
    Number(SyntaxToken),
    Str(SyntaxToken),
    Suffix(SuffixExpr),
    Table(TableCtor),
    Varargs(SyntaxToken),
}

impl AstNode for Value {
    fn children(&self) -> Vec<&dyn AstNode> {
        match self {
            Value::Bool(node) => vec![node],
            Value::Nil(node) => vec![node],
            Value::Number(node) => vec![node],
            Value::Str(node) => vec![node],
            Value::Suffix(node) => node.children(),
            Value::Table(node) => node.children(),
            Value::Varargs(node) => vec![node],
        }
    }

    fn location(&self) -> Location {
        match self {
            Value::Bool(node) => node.location(),
            Value::Nil(node) => node.location(),
            Value::Number(node) => node.location(),
            Value::Str(node) => node.location(),
            Value::Suffix(node) => node.location(),
            Value::Table(node) => node.location(),
            Value::Varargs(node) => node.location(),
        }
    }
}
