use super::*;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum CallArgs {
    ExprList {
        start_position: Position,
        list: ExprList,
        end_position: Position,
    },
    Str(SyntaxToken),
}

impl AstNode for CallArgs {
    fn children(&self) -> Vec<&dyn AstNode> {
        match self {
            CallArgs::ExprList { list, .. } => list.children(),
            CallArgs::Str(node) => node.children(),
        }
    }

    fn location(&self) -> Location {
        match self {
            CallArgs::ExprList {
                start_position,
                end_position,
                ..
            } => Location::new(*start_position, *end_position),
            CallArgs::Str(node) => node.location(),
        }
    }
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct FunctionCall {
    pub base: Box<SuffixExpr>,
    pub args: CallArgs,
}

impl AstNode for FunctionCall {
    fn children(&self) -> Vec<&dyn AstNode> {
        let mut children: Vec<&dyn AstNode> = vec![self.base.as_ref()];
        for arg in self.args.children() {
            children.push(arg);
        }
        children
    }

    fn location(&self) -> Location {
        Location::new(self.base.location().start, self.args.location().end)
    }
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct Paren {
    pub start_position: Position,
    pub expr: Box<Expr>,
    pub end_position: Position,
}

impl AstNode for Paren {
    fn children(&self) -> Vec<&dyn AstNode> {
        vec![self.expr.as_ref()]
    }

    fn location(&self) -> Location {
        Location::new(self.start_position, self.end_position)
    }
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum SuffixExpr {
    FunctionCall(FunctionCall),
    Paren(Paren),
    Var(VarExpr),
}

impl AstNode for SuffixExpr {
    fn children(&self) -> Vec<&dyn AstNode> {
        match self {
            SuffixExpr::FunctionCall(node) => node.children(),
            SuffixExpr::Paren(node) => node.children(),
            SuffixExpr::Var(node) => node.children(),
        }
    }

    fn location(&self) -> Location {
        match self {
            SuffixExpr::FunctionCall(node) => node.location(),
            SuffixExpr::Paren(node) => node.location(),
            SuffixExpr::Var(node) => node.location(),
        }
    }
}
