use crate::ast::*;
use super::*;

mod lassign;
mod vassign;

pub use lassign::*;
pub use vassign::*;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum Stmt {
    Call(FunctionCall),
    LocalAssignment(LocalAssignment),
    VarAssign(VarAssign),
}

impl AstNode for Stmt {
    fn children(&self) -> Vec<&dyn AstNode> {
        match self {
            Stmt::Call(node) => node.children(),
            Stmt::LocalAssignment(node) => node.children(),
            Stmt::VarAssign(node) => node.children(),
        }
    }

    fn location(&self) -> crate::ast::Location {
        match self {
            Stmt::Call(node) => node.location(),
            Stmt::LocalAssignment(node) => node.location(),
            Stmt::VarAssign(node) => node.location(),
        }
    }
}

pub type Stmts = NodeList<Stmt>;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum LastStmt {
    Break(SyntaxToken),
    Return {
        location: Location,
        exprlist: ExprList,
    },
}

impl AstNode for LastStmt {
    fn children(&self) -> Vec<&dyn AstNode> {
        match self {
            LastStmt::Break(node) => vec![node],
            LastStmt::Return { exprlist, .. } => exprlist.children(),
        }
    }

    fn location(&self) -> Location {
        match self {
            LastStmt::Break(node) => node.location(),
            LastStmt::Return { location, .. } => *location,
        }
    }
}
