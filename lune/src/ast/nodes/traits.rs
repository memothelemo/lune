use crate::ast::*;

pub trait AstNode {
    fn children(&self) -> Vec<&dyn AstNode>;
    fn location(&self) -> Location;
}

// pub trait AstToken<'a>: Sized {
// 	fn token(&self) -> &SyntaxToken;

// 	fn can_cast(typ: &'a TokenType) -> bool;
// 	fn cast(token: &'a SyntaxToken) -> Option<Self>;

// 	fn start_position(&self) -> Position {
// 		self.token().token.start_position
// 	}

// 	fn end_position(&self) -> Position {
// 		self.token().token.end_position
// 	}
// }
