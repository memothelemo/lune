use super::*;
use crate::ast::*;
use atom::Atom;

#[derive(Debug, Clone, PartialEq)]
pub enum TokenizeErrorType {
    IncompleteString,
    IncompleteComment,
    InvalidShebang,
    UnexpectedCharacter(char),
}

#[derive(Debug, Clone, PartialEq)]
pub struct TokenizeError {
    pub typ: TokenizeErrorType,
    pub position: Position,
}

type RawToken = Result<TokenType, TokenizeErrorType>;

impl From<TokenType> for RawToken {
    fn from(ty: TokenType) -> Self {
        Ok(ty)
    }
}

impl From<TokenizeErrorType> for RawToken {
    fn from(err: TokenizeErrorType) -> Self {
        Err(err)
    }
}

fn tokenize_atom(atom: Atom, slice: &str) -> RawToken {
    match atom {
        Atom::Comment => {
            // check for any multi-comment patterns there
            let equals = atom::get_equal_brackets(&slice[2..]);
            let len = slice.len();
            let multiline_len = equals.map(|v| v + 2).unwrap_or(0);
            let last_deduct = if slice.ends_with('\n') { 1 } else { 0 };
            TokenType::Comment {
                typ: match equals {
                    Some(e) => CommentType::Multiline(e),
                    None => CommentType::Single,
                },
                content: slice[2 + multiline_len..(len - multiline_len - last_deduct)].into(),
            }
            .into()
        }
        Atom::Number => TokenType::Number(slice.into()).into(),
        Atom::QuoteString => {
            let len = slice.len();
            TokenType::String {
                quote: StringQuoteType::Double,
                content: slice[1..len - 1].into(),
            }
            .into()
        }
        Atom::ApostropheString => {
            let len = slice.len();
            TokenType::String {
                quote: StringQuoteType::Single,
                content: slice[1..len - 1].into(),
            }
            .into()
        }
        Atom::BracketString => {
            let equals = atom::get_equal_brackets(slice).unwrap();
            let len = slice.len();
            TokenType::String {
                quote: StringQuoteType::Brackets(equals),
                content: slice[2 + equals..len - (2 + equals)].into(),
            }
            .into()
        }
        Atom::Whitespace => TokenType::Whitespace(slice.into()).into(),
        Atom::Unknown => match slice.chars().next().unwrap_or('\0') {
            '\'' | '"' | '[' => TokenizeErrorType::IncompleteString,
            '-' => TokenizeErrorType::IncompleteComment,
            '#' => TokenizeErrorType::InvalidShebang,
            c => TokenizeErrorType::UnexpectedCharacter(c),
        }
        .into(),
        Atom::Identifier => TokenType::Identifier(slice.into()).into(),
        _ => TokenType::Symbol(SymbolType::parse(slice).unwrap()).into(),
    }
}

macro_rules! next_if {
    ($lexer:expr, $cmp:expr) => {
        if $lexer.clone().next() == Some($cmp) {
            $lexer.next();
            Some($lexer.slice().into())
        } else {
            None
        }
    };
}

fn tokenize_code(input: &'_ str) -> Vec<(RawToken, usize)> {
    let mut lexer = Atom::lexer(input);
    let mut tokens = Vec::new();

    if let Some(text) = next_if!(lexer, Atom::Shebang) {
        tokens.push((TokenType::Shebang(text).into(), lexer.span().end))
    }

    while let Some(atom) = lexer.next() {
        tokens.push((tokenize_atom(atom, lexer.slice()), lexer.span().end));
    }

    tokens
}

pub fn tokenize(input: &'_ str) -> Result<Vec<Token>, TokenizeError> {
    let mut raw_tokens = tokenize_code(input);
    let mut raw_tokens = raw_tokens.drain(..);

    let mut position = Position::default();
    let mut start_position = position;

    let mut tokens = Vec::new();

    if let Some((mut token_type, mut token_offset)) = raw_tokens.next() {
        for character in input.chars() {
            let next_is_new_line = if character == '\n' {
                true
            } else {
                position.column += 1;
                false
            };

            position.offset += character.len_utf8();

            if next_is_new_line {
                position.line += 1;
                position.column = 1;
            }

            let end_position = position;
            if token_offset == end_position.offset {
                tokens.push(Token {
                    start_position,
                    token_type: token_type.map_err(|typ| TokenizeError {
                        position: start_position,
                        typ,
                    })?,
                    end_position,
                });
                start_position = position;

                if let Some((next_token_type, next_token_offset)) = raw_tokens.next() {
                    token_type = next_token_type;
                    token_offset = next_token_offset;
                } else {
                    break;
                }
            }
        }
    }

    if raw_tokens.next().is_none() {
        tokens.push(Token {
            start_position: position,
            token_type: TokenType::Eof,
            end_position: position,
        });
    } else {
        panic!("Leftover token!");
    }

    Ok(tokens)
}
