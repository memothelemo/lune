use criterion::{black_box, criterion_group, criterion_main, Criterion};
use lune::syntax::tokenizer;

static SOURCE: &str = include_str!("date.le");

fn tokenize(c: &mut Criterion) {
    c.bench_function("tokenize date", |b| {
        b.iter(|| tokenizer::tokenize(black_box(SOURCE)));
    });
}

criterion_group! {
    name = benches;
    config = Criterion::default().sample_size(20);
    targets = tokenize
}

criterion_main!(benches);
