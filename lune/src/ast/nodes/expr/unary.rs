use super::*;
use crate::operator;

operator! {
    #[allow(dead_code)]
    #[derive(Debug, Clone)]
    pub enum UnopKind {
        Length => 7,
        Negate => 7,
        Not => 7,
    }
    is_right_associate = |_| false
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct UnaryOperator {
    pub kind: UnopKind,
    pub token: SyntaxToken,
}

impl AstNode for UnaryOperator {
    fn children(&self) -> Vec<&dyn AstNode> {
        self.token.children()
    }

    fn location(&self) -> Location {
        self.token.location()
    }
}
