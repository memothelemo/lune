use crate::ast::*;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct NodeList<T: AstNode> {
    pub entries: Vec<T>,
}

impl<T: AstNode> AstNode for NodeList<T> {
    fn children(&self) -> Vec<&dyn AstNode> {
        let mut nodes: Vec<&dyn AstNode> = Vec::new();
        for entry in self.entries.iter() {
            nodes.push(entry);
        }
        nodes
    }

    fn location(&self) -> Location {
        if self.entries.is_empty() {
            Location::empty()
        } else {
            let children = self.children();
            Location::new(
                children.first().unwrap().location().start,
                children.last().unwrap().location().end,
            )
        }
    }
}

pub type ExprList = NodeList<Expr>;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum ParameterName {
    Name(SyntaxToken),
    Varargs(SyntaxToken),
}

impl AstNode for ParameterName {
    fn children(&self) -> Vec<&dyn AstNode> {
        vec![match self {
            ParameterName::Name(node) => node,
            ParameterName::Varargs(node) => node,
        }]
    }

    fn location(&self) -> Location {
        match self {
            ParameterName::Name(node) => node,
            ParameterName::Varargs(node) => node,
        }
        .location()
    }
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct Parameter {
    pub name: ParameterName,
    pub default: Option<Expr>,
}

impl AstNode for Parameter {
    fn children(&self) -> Vec<&dyn AstNode> {
        let mut collection: Vec<&dyn AstNode> = vec![&self.name];
        if let Some(default) = self.default.as_ref() {
            collection.push(default);
        }
        collection
    }

    fn location(&self) -> Location {
        let children = self.children();
        Location::new(
            children.first().unwrap().location().start,
            children.last().unwrap().location().end,
        )
    }
}

pub type ParameterList = NodeList<Parameter>;
