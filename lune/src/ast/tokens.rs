use super::*;
use crate::lazy_enum;

mod traits;
mod types;
mod variants;

pub use traits::*;
pub use types::*;
pub use variants::*;

#[derive(Debug, Clone, PartialEq)]
pub enum TokenKind {
    Eof,
    Comment,
    Keyword,
    Identifier,
    Number,
    Shebang,
    Symbol,
    String,
    Whitespace,
}

#[derive(Debug, Clone, PartialEq)]
pub enum TokenType {
    /// End of the file or the text.
    Eof,

    /// Ignored token of them all. It must be considered as a trivia or notice.
    Comment { typ: CommentType, content: SmolStr },

    /// A variable or name token
    Identifier(SmolStr),

    /// Any kind of number. Lua numbers are complex
    Number(SmolStr),

    /// It may be found in the first line of the script file.
    /// It tells what script interpreter should be used (a feature in Linux).
    Shebang(SmolStr),

    /// The rest of the letters and some words like `local` valid in Lua is considered as symbol.
    Symbol(SymbolType),

    /// Any kind of string that can be valid in Lua
    String {
        quote: StringQuoteType,
        content: SmolStr,
    },

    /// Necessary for identation or the syntax of the language
    Whitespace(SmolStr),
}

impl TokenType {
    pub fn is_eof(&self) -> bool {
        matches!(self, TokenType::Eof)
    }

    pub fn is_trivia(&self) -> bool {
        matches!(
            self,
            TokenType::Comment { .. } | TokenType::Shebang(..) | TokenType::Whitespace(..)
        )
    }

    pub fn kind(&self) -> TokenKind {
        match self {
            TokenType::Eof => TokenKind::Eof,
            TokenType::Comment { .. } => TokenKind::Comment,
            TokenType::Identifier(_) => TokenKind::Identifier,
            TokenType::Number(_) => TokenKind::Number,
            TokenType::Shebang(_) => TokenKind::Shebang,
            TokenType::Symbol(_) => TokenKind::Symbol,
            TokenType::String { .. } => TokenKind::String,
            TokenType::Whitespace(_) => TokenKind::Whitespace,
        }
    }
}

impl std::fmt::Display for TokenType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            TokenType::Eof => "<eof>".into(),
            TokenType::Comment { typ, content } => format!(
                "{}{}{}",
                typ.start_quote(),
                content.escape_default(),
                typ.end_quote()
            ),
            TokenType::Identifier(i) => i.to_string(),
            TokenType::Number(n) => n.to_string(),
            TokenType::Shebang(s) => format!("#!{}", s.escape_default()),
            TokenType::Symbol(s) => s.to_string(),
            TokenType::String { quote, content } => format!(
                "{}{}{}",
                quote.start_quote(),
                content.escape_default(),
                quote.end_quote()
            ),
            TokenType::Whitespace(w) => w.escape_default().to_string(),
        }
        .fmt(f)
    }
}

#[derive(Clone, PartialEq)]
pub struct Token {
    pub(crate) start_position: Position,
    pub(crate) token_type: TokenType,
    pub(crate) end_position: Position,
}

impl Token {
    pub fn start_position(&self) -> Position {
        self.start_position
    }

    pub fn end_position(&self) -> Position {
        self.end_position
    }

    pub fn token_type(&self) -> &TokenType {
        &self.token_type
    }

    pub fn token_kind(&self) -> TokenKind {
        self.token_type.kind()
    }

    pub fn is_trivia(&self) -> bool {
        self.token_type.is_trivia()
    }

    pub fn is_eof(&self) -> bool {
        self.token_type.is_eof()
    }
}

impl std::fmt::Debug for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Token({} - {},`{}`)",
            self.start_position, self.end_position, self.token_type
        )
    }
}

impl std::fmt::Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.token_type.fmt(f)
    }
}

#[derive(Clone, PartialEq)]
pub struct SyntaxToken {
    pub leadings: Vec<Token>,
    pub token: Token,
}

impl SyntaxToken {
    pub fn start_position(&self) -> Position {
        self.token.start_position
    }

    pub fn end_position(&self) -> Position {
        self.token.end_position
    }

    pub fn token_type(&self) -> &TokenType {
        &self.token.token_type
    }

    pub fn token_kind(&self) -> TokenKind {
        self.token.token_kind()
    }
}

impl crate::ast::AstNode for SyntaxToken {
    fn children(&self) -> Vec<&dyn AstNode> {
        vec![self]
    }

    fn location(&self) -> Location {
        Location::new(self.token.start_position, self.token.end_position)
    }
}

impl std::fmt::Debug for SyntaxToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_list()
            .entries(&self.leadings)
            .entry(&self.token)
            .finish()
    }
}

impl std::fmt::Display for SyntaxToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for leading in self.leadings.iter() {
            write!(f, "{}", leading)?;
        }
        write!(f, "{}", self.token)
    }
}

/// Converts all of the raw tokens to syntax tokens. Useful before parsing.
pub fn to_syntax_tokens(mut raw_tokens: Vec<Token>) -> Vec<SyntaxToken> {
    let mut leadings = Vec::new();
    let mut tokens = Vec::new();

    let raw_tokens = raw_tokens.drain(..).peekable();

    for token in raw_tokens {
        if token.is_trivia() {
            leadings.push(token);
            continue;
        }
        tokens.push(SyntaxToken { leadings, token });
        leadings = Vec::new();
    }

    tokens
}
