use super::*;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum VarAssignName {
    AccessExpr(AccessExpr),
    AccessName(AccessName),
    Name(SyntaxToken),
    Varargs(SyntaxToken),
}

impl AstNode for VarAssignName {
    fn children(&self) -> Vec<&dyn AstNode> {
        match self {
            VarAssignName::AccessExpr(node) => node.children(),
            VarAssignName::AccessName(node) => node.children(),
            VarAssignName::Name(node) => vec![node],
            VarAssignName::Varargs(node) => node.children(),
        }
    }

    fn location(&self) -> Location {
        match self {
            VarAssignName::AccessExpr(node) => node.location(),
            VarAssignName::AccessName(node) => node.location(),
            VarAssignName::Name(node) => node.location(),
            VarAssignName::Varargs(node) => node.location(),
        }
    }
}

pub type VarAssignNames = NodeList<VarAssignName>;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct VarAssign {
    pub names: VarAssignNames,
    pub exprlist: ExprList,
}

impl AstNode for VarAssign {
    fn children(&self) -> Vec<&dyn AstNode> {
        let mut children: Vec<&dyn AstNode> = self.names.children();
        for child in self.exprlist.children() {
            children.push(child);
        }
        children
    }

    fn location(&self) -> Location {
        Location::new(
            self.names.location().start,
            self.exprlist.location().end
        )
    }
}
