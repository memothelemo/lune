use super::*;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub enum VarExpr {
    AccessExpr(AccessExpr),
    AccessMethod(AccessMethod),
    AccessName(AccessName),
    Name(SyntaxToken),
}

impl AstNode for VarExpr {
    fn children(&self) -> Vec<&dyn AstNode> {
        match self {
            VarExpr::AccessExpr(node) => node.children(),
            VarExpr::AccessMethod(node) => node.children(),
            VarExpr::AccessName(node) => node.children(),
            VarExpr::Name(node) => vec![node],
        }
    }

    fn location(&self) -> Location {
        match self {
            VarExpr::AccessExpr(node) => node.location(),
            VarExpr::AccessMethod(node) => node.location(),
            VarExpr::AccessName(node) => node.location(),
            VarExpr::Name(node) => node.location(),
        }
    }
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct AccessMethod {
    pub suffix: Box<SuffixExpr>,
    pub indexer: SyntaxToken,
}

impl AstNode for AccessMethod {
    fn children(&self) -> Vec<&dyn AstNode> {
        let mut children: Vec<&dyn AstNode> = self.suffix.children();
        children.push(&self.indexer);
        children
    }

    fn location(&self) -> Location {
        Location::new(
            self.suffix.location().start,
            self.indexer.location().end
        )
    }
}


#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct AccessName {
    pub suffix: Box<SuffixExpr>,
    pub indexer: SyntaxToken,
}

impl AstNode for AccessName {
    fn children(&self) -> Vec<&dyn AstNode> {
        let mut children: Vec<&dyn AstNode> = self.suffix.children();
        children.push(&self.indexer);
        children
    }

    fn location(&self) -> Location {
        Location::new(
            self.suffix.location().start,
            self.indexer.location().end
        )
    }
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct AccessExpr {
    pub location: Location,
    pub suffix: Box<SuffixExpr>,
    pub expr: Box<Expr>,
}

impl AstNode for AccessExpr {
    fn children(&self) -> Vec<&dyn AstNode> {
        let mut children: Vec<&dyn AstNode> = self.suffix.children();
        for child in self.expr.as_ref().children() {
            children.push(child);
        }
        children
    }

    fn location(&self) -> Location {
        self.location.clone()
    }
}
